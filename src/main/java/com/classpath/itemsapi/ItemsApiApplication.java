package com.classpath.itemsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication
public class ItemsApiApplication{

    public static void main(String[] args) {
        SpringApplication.run(ItemsApiApplication.class, args);


    }

    /*@Override
    public void run(String... args) throws Exception {
        ItemService itemService = applicationContext.getBean("itemService", ItemService.class);

        itemService.saveItem( Item.builder()
                .itemId(12)
                .price(23_000)
                .name("IPad").build());
    }*/
}
