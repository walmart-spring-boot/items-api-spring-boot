package com.classpath.itemsapi.service;

import com.classpath.itemsapi.model.Item;
import com.classpath.itemsapi.repository.ItemDAO;
import com.classpath.itemsapi.repository.ItemRepository;
import lombok.AllArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service("itemService")
@AllArgsConstructor
//@Profile({"dev", "qa", "pre-prod"})
@Primary
public class ItemServiceImpl implements ItemService {

   // private ItemDAO itemDAO;
    @Autowired
    private ItemRepository itemRepository;

/*
    public ItemServiceImpl(ItemDAO itemDAO){
        this.itemDAO = itemDAO;
    }*/

    @Override
    public Item saveItem(Item item) {
        return this.itemRepository.save(item);
    }

    @Override
    public Set<Item> fetchItems() {
        return new HashSet<>(this.itemRepository.findAll());
    }

    @Override
    public Item fetchItemById(long itemId) {
        return this.itemRepository.getOne(itemId);
    }

    @Override
    public void deleteItemById(long itemId) {
        this.itemRepository.deleteById(itemId);
    }
}